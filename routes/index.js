var express = require('express');
var router = express.Router();


router.get('*', function (req, res, next) {

	var msgObj = {};
	msgObj.code = "404";
	msgObj.result = "invalid request";   
	res.end(JSON.stringify(msgObj));
});

router.post('*', function (req, res, next) {

	var msgObj = {};
	msgObj.code = "404";
	msgObj.result = "invalid request";   
	res.end(JSON.stringify(msgObj));
});

module.exports = router;

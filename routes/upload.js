var express = require('express');
var multiparty = require('multiparty');
var fs = require('fs');
var Lame = require("node-lame").Lame;
var router = express.Router();

router.post('/', function(req, res, next) {

	var form = new multiparty.Form();
	var filename;

	// get field name & value
	form.on('field',function(name,value) {

		console.log('normal field / name = '+name+' , value = '+value);
		if ( name == 'filename') filename = value;
	});

    // file upload handling
	form.on('part',function(part) {

		/*
		var filename;
		var size;

		if (part.filename) {

			filename = part.filename;
			size = part.byteCount;

		}
		else{

			part.resume();
		}*/

		console.log("Write Streaming file :"+filename);
		var writeStream = fs.createWriteStream('./audiofiles/'+filename);
		writeStream.filename = filename;
		part.pipe(writeStream);

		part.on('data',function(chunk){

			console.log(filename+' read '+chunk.length + 'bytes');
		});

		part.on('end',function(){

			console.log(filename+' Part read complete');
			writeStream.end();
		});
	});

	// all uploads are completed
	form.on('close',function() {

		const encoder = new Lame({
			output: "./audiofiles/" + filename + ".mp3",
			bitrate: 192,
			resample : 16}).setFile("./audiofiles/" + filename);

		encoder.encode().then(() => {
        	// Encoding finished
	        var msgObj = {};
			msgObj.code = "200";
			msgObj.result = "completed";
			msgObj.urlPath = "/audiofiles/" + filename + ".mp3";

			fs.unlinkSync('./audiofiles/'+filename);

			res.writeHead(200, {
			
				'content-type' : 'application/json',
				'Access-Control-Allow-Origin' : '*',
				'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, DELETE',
				'Access-Control-Max-Age' : '3600',
				'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept, X-CSRF-Token, Access-Control-Allow-Headers '
			});

			res.end(JSON.stringify(msgObj));
    	}).catch(error => {

        // Something went wrong
        	var msgObj = {};
			msgObj.code = "500";
			msgObj.result = error;

			res.writeHead(200, {
			
				'content-type' : 'application/json',
				'Access-Control-Allow-Origin' : '*',
				'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, DELETE',
				'Access-Control-Max-Age' : '3600',
				'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept, X-CSRF-Token, Access-Control-Allow-Headers '
			});

			res.end(JSON.stringify(msgObj));
    	});

		
	});

	// track progress
	form.on('progress',function(byteRead,byteExpected){

		console.log(' Reading total  '+byteRead+'/'+byteExpected);
	});

	form.parse(req);	
});

module.exports = router;

